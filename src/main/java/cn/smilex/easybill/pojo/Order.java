package cn.smilex.easybill.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * @author smilex
 */
@Data
@AllArgsConstructor
public class Order {
    private Long user_id;
    private Long order_id;
    private Date order_time;
    private Double amount;

    private User user;

    public Order() {
    }
}
