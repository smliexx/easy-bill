<%--
  Created by IntelliJ IDEA.
  User: s's
  Date: 2022/2/25
  Time: 20:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>登录</title>
</head>
<body>

<div class="wrap">
    <input type="text" id="userAccount" name="userAccount" placeholder="userAccount"/> <br>
    <input type="password" id="passWord" name="passWord" placeholder="passWord"/> <br>
    <button id="login">登录</button>
    <button id="register">注册</button>
</div>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(function () {
        $("#login").click(function () {
            let userAccount = $("#userAccount").val()
            let passWord = $("#passWord").val()

            $.ajax({
                url: "/api/login",
                type: "POST",
                dataType: "JSON",
                data: {
                    userAccount: userAccount,
                    passWord: passWord
                },
                success: function (data) {
                    console.log(data)
                    if (data.code === 200) {
                        location.href = "${pageContext.request.contextPath}/"
                    }
                }
            })
        })

        $("#register").click(function () {
            location.href = "${pageContext.request.contextPath}/register"
        })
    })
</script>
</body>
</html>
