package cn.smilex.easybill.controller;

import cn.smilex.easybill.pojo.Msg;
import cn.smilex.easybill.service.UserService;
import cn.smilex.easybill.util.EasyBillUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author smilex
 */
@Controller
public class LoginController {

    private UserService userService;

    /**
     * 登陆页面
     */
    @GetMapping("/login")
    public String loginPage(HttpSession session) {
        if (session.getAttribute("realUser") != null) {
            return "redirect:/";
        }
        return "login";
    }

    /**
     * 登陆API
     *
     * @param request 请求对象
     * @param session 会话对象
     * @return Msg
     */
    @ResponseBody
    @PostMapping(value = "/api/login", produces = "text/plain;charset=UTF-8")
    public String loginApi(HttpServletRequest request, HttpSession session) {

        Msg msg = new Msg(500, "登录失败!");

        String userAccount = request.getParameter("userAccount");
        String passWord = request.getParameter("passWord");

        if (userAccount != null && !userAccount.isBlank() &&
                passWord != null && !passWord.isBlank()) {

            var user = userService.selectByUserAccountAndPassWord(userAccount, passWord);
            if (user != null) {
                msg.setCode(200);
                msg.setMsg("登录成功!");
                session.setAttribute("realUser", user);
            }
        }

        return EasyBillUtil.objectToJsonStr(msg);
    }

    /**
     * 退出登录
     *
     * @param session 会话
     * @return 登录页面
     */
    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.setAttribute("realUser", null);
        return "redirect:/login";
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
