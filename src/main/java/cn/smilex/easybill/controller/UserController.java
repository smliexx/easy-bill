package cn.smilex.easybill.controller;

import cn.smilex.easybill.pojo.Msg;
import cn.smilex.easybill.service.UserService;
import cn.smilex.easybill.util.EasyBillUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author smilex
 */
@Controller
public class UserController {

    private UserService userService;

    @GetMapping("/api/user/del")
    public String deleteUserApi(HttpSession session,
                                @RequestParam(value = "userId", required = false) Integer userId) {

        if (session.getAttribute("realUser") != null) {
            if (userId != null && userId >= 0) {
                if (userService.deleteByUserId(userId)) {
                    System.out.println("删除用户成功!");
                } else {
                    System.out.println("删除用户失败!");
                }
            }
        }

        return "redirect:/";
    }

    /**
     * 通过用户名查询是否被注册
     *
     * @param userName 用户名
     */
    @ResponseBody
    @GetMapping(value = "/api/user/is", produces = "text/plan;charset=utf-8")
    public String isUser(@RequestParam(value = "userName", required = false) String userName) {
        Msg msg = new Msg(500, "名称已存在!");

        if (userName != null) {
            if (userService.selectByUserNameCount(userName) <= 0) {
                msg.setCode(200);
                msg.setMsg("名称可以使用!");
            }
        }

        return EasyBillUtil.objectToJsonStr(msg);
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
