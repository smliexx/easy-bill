package cn.smilex.easybill.service.impl;

import cn.smilex.easybill.dao.UserDao;
import cn.smilex.easybill.pojo.User;
import cn.smilex.easybill.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author smilex
 */
@Service
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Transactional(readOnly = true)
    @Override
    public List<User> selectAll() {
        return userDao.selectAll();
    }

    @Transactional(readOnly = true)
    @Override
    public User selectByUserAccountAndPassWord(String userAccount, String passWord) {
        return userDao.selectByUserAccountAndPassWord(userAccount, passWord);
    }

    @Transactional
    @Override
    public boolean deleteByUserId(Integer userId) {
        return userDao.deleteByUserId(userId) > 0;
    }

    @Transactional
    @Override
    public int selectByUserNameCount(String userName) {
        return userDao.selectByUserNameCount(userName);
    }

    @Transactional
    @Override
    public boolean insertUser(User user) {
        return userDao.insertUser(user) > 0;
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
