package cn.smilex.easybill.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author smilex
 */
@Data
@AllArgsConstructor
public class User {

    private long user_id;
    private String user_account;
    private String user_name;
    private String password;
    private String email;

    public User() {
    }
}
