package cn.smilex.easybill.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author smilex
 */
@Data
@AllArgsConstructor
public class Msg {
    private Integer code;
    private String msg;

    public Msg() {
    }
}
