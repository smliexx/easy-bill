package cn.smilex.easybill.controller;

import cn.smilex.easybill.service.OrderService;
import cn.smilex.easybill.util.EasyBillUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @author smilex
 */
@Controller
public class OrderController {
    private OrderService orderService;

    /**
     * 订单修改页面
     */
    @GetMapping("/modify")
    public String modifyPage(HttpServletRequest request,
                             @RequestParam(value = "orderId", required = false) Integer orderId) {
        request.setAttribute("orderId", orderId);
        return "modify";
    }

    /**
     * 删除订单
     *
     * @param orderId 订单ID
     * @return 返回首页
     */
    @GetMapping("/api/order/del")
    public String deleteUserApi(HttpSession session,
                                @RequestParam(value = "orderId", required = false) Integer orderId) {

        if (session.getAttribute("realUser") != null) {
            if (orderId != null && orderId >= 0) {
                if (orderService.deleteByOrderId(orderId)) {
                    System.out.println("删除订单成功!");
                } else {
                    System.out.println("删除订单失败!");
                }
            }
        }

        return "redirect:/";
    }

    /**
     * 更改订单的用户id
     *
     * @return 返回首页
     */
    @GetMapping("/api/order/mofiy")
    public String modifyOrderUser(HttpSession session,
                                  @RequestParam(value = "orderId", required = false) Integer orderId,
                                  @RequestParam(value = "userId", required = false) Integer userId) {

        if (session.getAttribute("realUser") != null) {
            if (orderId != null && userId != null && orderId >= 0 && userId >= 0) {
                if (orderService.modifyOrderUserId(orderId, userId)) {
                    System.out.println("修改订单用户成功!");
                } else {
                    System.out.println("修改订单用户失败!");
                }
            }
        }

        return "redirect:/";
    }


    @ResponseBody
    @PostMapping("/api/order/search")
    public String searchOrder(HttpSession session,
                              @RequestParam(value = "userAccount", required = false) String userAccount,
                              @RequestParam(value = "leftTime", required = false) String _leftTime,
                              @RequestParam(value = "rightTime", required = false) String _rightTime,
                              @RequestParam(value = "leftMoney", required = false) String _leftMoney,
                              @RequestParam(value = "rightMoney", required = false) String _rightMoney) {
        if (session.getAttribute("realUser") != null) {
            if (_leftTime != null && _rightTime != null && _leftMoney != null && _rightMoney != null) {
                try {
                    Date leftTime = EasyBillUtil.stringToDate(_leftTime);
                    Date rightTime = EasyBillUtil.stringToDate(_rightTime);
                    Double leftMoney = Double.parseDouble(_leftMoney);
                    Double rightMoney = Double.parseDouble(_rightMoney);

                    System.out.println(userAccount);
                    System.out.println(leftTime);
                    System.out.println(rightTime);
                    System.out.println(leftMoney);
                    System.out.println(rightMoney);


                    session.setAttribute("why", orderService.selectByWhy(userAccount, leftTime, rightTime, leftMoney, rightMoney));

                    return "ok";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return "error";
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
