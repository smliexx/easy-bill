package cn.smilex.easybill.service;

import cn.smilex.easybill.pojo.User;

import java.util.List;

/**
 * @author smilex
 */
public interface UserService {

    List<User> selectAll();

    User selectByUserAccountAndPassWord(String userAccount, String passWord);

    boolean deleteByUserId(Integer userId);

    int selectByUserNameCount(String userName);

    boolean insertUser(User user);
}
