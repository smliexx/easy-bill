package cn.smilex.easybill.dao;

import cn.smilex.easybill.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author smilex
 */
@Repository
public interface UserDao {

    List<User> selectAll();

    User selectByUserAccountAndPassWord(@Param("userAccount") String userAccount,
                                        @Param("passWord") String passWord);

    int deleteByUserId(Integer userId);

    int selectByUserNameCount(String userName);

    int insertUser(User user);
}
