package cn.smilex.easybill.controller;

import cn.smilex.easybill.pojo.Order;
import cn.smilex.easybill.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author smilex
 */
@Controller
public class IndexController {

    private OrderService orderService;

    /**
     * 首页
     */
    @GetMapping("/")
    public String indexPage(HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("realUser") != null) {

            Object why = session.getAttribute("why");

            if (why != null) {
                request.setAttribute("orders", why);
                session.setAttribute("why", null);
            } else {
                request.setAttribute("orders", orderService.selectAll());
            }

            return "index";
        }
        return "redirect:/login";
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
