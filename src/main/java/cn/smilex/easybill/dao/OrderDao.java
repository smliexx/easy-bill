package cn.smilex.easybill.dao;

import cn.smilex.easybill.pojo.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author smilex
 */
@Repository
public interface OrderDao {

    List<Order> selectAll();

    int deleteByOrderId(Integer orderId);

    int modifyOrderUserId(@Param("orderId") Integer orderId,
                          @Param("userId") Integer userId);

    List<Order> selectByWhy(@Param("userAccount") String userAccount,
                            @Param("leftTime") Date leftTime,
                            @Param("rightTime") Date rightTime,
                            @Param("leftMoney") Double leftMoney,
                            @Param("rightMoney") Double rightMoney);
}
