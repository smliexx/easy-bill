package cn.smilex.easybill.util;

import cn.smilex.blacksky.jni.json.JsonMut;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author smilex
 */
public final class EasyBillUtil {
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-hh HH:mm");

    /**
     * 对象转换到Json字符串
     *
     * @return Json字符串
     */
    public static String objectToJsonStr(Object obj) {

        JsonMut mut = JsonMut.buildObject();
        var root = (JsonMut.JsonMutObject) mut.getRoot();

        try {
            Class<?> aClass = obj.getClass();
            Field[] fields = aClass.getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                Class<?> type = field.getType();

                if (type == int.class || type == Integer.class) {
                    root.addInt(field.getName(), (Integer) field.get(obj));
                } else if (type == double.class || type == Double.class) {
                    root.addDouble(field.getName(), (Double) field.get(obj));
                } else if (type == long.class || type == Long.class) {
                    root.addLong(field.getName(), (Long) field.get(obj));
                } else if (type == String.class) {
                    root.addStr(field.getName(), (String) field.get(obj));
                }
            }

            return mut.getJsonStr();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            mut.close();
        }
    }

    /**
     * 将Date转换为字符串
     *
     * @param date 日期
     * @return 日期字符串
     */
    public static String dateToString(Date date) {
        return simpleDateFormat.format(date);
    }

    /**
     * 将日期字符串(datetime-local)转换为Date
     *
     * @param str 日期租房处
     * @return Date
     */
    public static Date stringToDate(String str) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(str);
    }
}
