<%@ page import="java.util.List" %>
<%@ page import="cn.smilex.easybill.pojo.Order" %>
<%@ page import="cn.smilex.easybill.util.EasyBillUtil" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: s's
  Date: 2022/2/26
  Time: 8:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<%
    List<Order> orders = (List<Order>) request.getAttribute("orders");
    if (orders == null) {
        orders = new ArrayList<>();
    }
%>

<label for="userAccount">用户账户: </label>
<input type="text" id="userAccount"> <br>

<label for="leftTime">下单时间: </label>
<input type="datetime-local" id="leftTime">
<label for="rightTime"> - </label>
<input type="datetime-local" id="rightTime" style="margin-right: 50px;">

<label for="leftMoney">订单金额: </label>
<input type="text" id="leftMoney">
<label for="rightMoney"> - </label>
<input type="text" id="rightMoney" style="margin-right: 50px;">

<button id="search">查询</button>
<hr>
<table>
    <tr>
        <th>用户ID</th>
        <th>用户账户</th>
        <th>用户名</th>
        <th>用户邮箱</th>
        <th>订单编号</th>
        <th>下单时间</th>
        <th>订单金额</th>
        <th>用户操作</th>
    </tr>
    <% for (Order order : orders) { %>
    <tr>
        <td><%= order.getUser().getUser_id() %>
        </td>
        <td><%= order.getUser().getUser_account()%>
        </td>
        <td><%= order.getUser().getUser_name() %>
        </td>
        <td><%= order.getUser().getEmail() %>
        </td>
        <td><%= order.getOrder_id() %>
        </td>
        <td><%= EasyBillUtil.dateToString(order.getOrder_time()) %>
        </td>
        <td><%= order.getAmount() %>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/modify?orderId=<%= order.getOrder_id() %>">修改</a>
            <a href="${pageContext.request.contextPath}/api/order/del?orderId=<%= order.getOrder_id() %>">删除</a>
        </td>
    </tr>
    <% } %>
</table>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(function () {
        $("#search").click(function () {
            let userAccount = $("#userAccount").val()

            let leftTime = $("#leftTime").val()
            let rightTime = $("#rightTime").val()

            let leftMoney = $("#leftMoney").val()
            let rightMoney = $("#rightMoney").val()

            $.ajax({
                url: "/api/order/search",
                async: false,
                method: "POST",
                dataType: "JSON",
                data: {
                    userAccount: userAccount,
                    leftTime: leftTime,
                    rightTime: rightTime,
                    leftMoney: leftMoney,
                    rightMoney: rightMoney,
                },
                success: function (data) {
                    console.log(data)
                }
            })

            location.reload()
        })
    })
</script>
</body>
</html>
