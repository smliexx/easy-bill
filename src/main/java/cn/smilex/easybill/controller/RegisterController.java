package cn.smilex.easybill.controller;

import cn.smilex.easybill.pojo.Msg;
import cn.smilex.easybill.pojo.User;
import cn.smilex.easybill.service.UserService;
import cn.smilex.easybill.util.EasyBillUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * @author smilex
 */
@Controller
public class RegisterController {

    private UserService userService;

    @GetMapping("/register")
    public String registerPage(HttpSession session) {
        if (session.getAttribute("realUser") != null) {
            return "redirect:/";
        }

        return "register";
    }

    /**
     * 添加用户api
     */
    @ResponseBody
    @PostMapping("/api/user/add")
    public String registerUser(@RequestParam(value = "userAccount", required = false) String userAccount,
                               @RequestParam(value = "userName", required = false) String userName,
                               @RequestParam(value = "passWord", required = false) String passWord,
                               @RequestParam(value = "email", required = false) String email) {
        Msg msg = new Msg(500, "注册失败!");

        if (userName != null && passWord != null && email != null) {

            System.out.println(email);
            User user = new User(0, userAccount, userName, passWord, email);

            if (userService.insertUser(user)) {
                msg.setCode(200);
                msg.setMsg("注册成功!");
            }
        }

        return EasyBillUtil.objectToJsonStr(msg);
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
