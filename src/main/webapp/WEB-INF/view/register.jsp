<%--
  Created by IntelliJ IDEA.
  User: s's
  Date: 2022/2/26
  Time: 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
</head>
<body>
<input type="text" value="" id="user_account" placeholder="用户账户"/> <br>
<input type="text" value="" id="user_name" placeholder="用户姓名"/> <br>
<input type="text" value="" id="password" placeholder="密码"/> <br>
<input type="text" value="" id="password2" placeholder="确认"/> <br>
<input type="text" value="" id="email" placeholder="邮箱"/> <br>
<button id="register">注册</button>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>

    let c = undefined
    let reg = /^\w+((.\w+)|(-\w+))@[A-Za-z0-9]+((.|-)[A-Za-z0-9]+).[A-Za-z0-9]+$/;

    $(function () {
        $("#register").click(function () {
            console.log(check())
        })
    })

    function isUserName(userName) {
        $.ajax({
            url: "/api/user/is",
            method: "GET",
            async: false,
            dataType: "JSON",
            data: {
                userName: userName,
            },
            success: function (data) {
                c = data.code == 200
            }
        })
    }

    function check() {
        let userAccount = $("#user_account").val()
        let userName = $("#user_name").val()
        let passWord = $("#password").val()
        let passWord2 = $("#password2").val()
        let email = $("#email").val()

        isUserName(userName)

        if (userAccount == '' || userName == '' || passWord == '' || passWord2 == '' || email == '') {
            return {
                code: 500,
                error: "参数为空"
            }
        } else if (!c) {
            return {
                code: 500,
                error: "用户名已存在"
            }
        } else if (passWord != passWord2) {
            return {
                code: 500,
                error: "两次密码不一致"
            }
        } else if (!reg.test(email)) {
            return {
                code: 500,
                error: "请输入正确的邮箱"
            }
        } else {
            $.ajax({
                url: "/api/user/add",
                method: "POST",
                dataType: "JSON",
                data: {
                    userAccount: userAccount,
                    userName: userName,
                    passWord: passWord,
                    email: email
                },
                success: function (data) {
                    console.log(data)
                    if (data.code == 200) {
                        location.href = "/login"
                    }
                }
            })
        }
    }
</script>
</body>
</html>
