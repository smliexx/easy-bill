package cn.smilex.easybill.service;

import cn.smilex.easybill.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author smilex
 */
public interface OrderService {

    List<Order> selectAll();

    boolean deleteByOrderId(Integer orderId);

    boolean modifyOrderUserId(Integer orderId,
                              Integer userId);

    List<Order> selectByWhy(String userAccount,
                            Date leftTime,
                            Date rightTime,
                            Double leftMoney,
                            Double rightMoney);
}
