<%--
  Created by IntelliJ IDEA.
  User: s's
  Date: 2022/2/26
  Time: 9:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>修改</title>
</head>
<body>
<input type="text" id="orderId" value="${orderId}" readonly="readonly" placeholder="orderId"/> <br>
<input type="text" id="userId" placeholder="userId"/> <br>
<button id="modify">修改</button>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(function () {
        $("#modify").click(function () {
            let orderId = $("#orderId").val()
            let userId = $("#userId").val()

            $.ajax({
                url: "/api/order/mofiy",
                type: "GET",
                data: {
                    orderId: orderId,
                    userId: userId,
                },
                success: function () {
                    location.href = "/"
                }
            })
        })
    })
</script>
</body>
</html>
