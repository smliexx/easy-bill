package cn.smilex.easybill.service.impl;

import cn.smilex.easybill.dao.OrderDao;
import cn.smilex.easybill.pojo.Order;
import cn.smilex.easybill.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author smilex
 */
@Service
public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;

    @Transactional(readOnly = true)
    @Override
    public List<Order> selectAll() {
        return orderDao.selectAll();
    }

    @Transactional
    @Override
    public boolean deleteByOrderId(Integer orderId) {
        return orderDao.deleteByOrderId(orderId) > 0;
    }

    @Transactional
    @Override
    public boolean modifyOrderUserId(Integer orderId, Integer userId) {
        return orderDao.modifyOrderUserId(orderId, userId) > 0;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Order> selectByWhy(String userAccount, Date leftTime, Date rightTime, Double leftMoney, Double rightMoney) {
        return orderDao.selectByWhy(userAccount, leftTime, rightTime, leftMoney, rightMoney);
    }

    @Autowired
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }
}
